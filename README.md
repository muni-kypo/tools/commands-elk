# ELK for the Analysis of Shell Commands From Cybersecurity Training

This project complements the [repository of shell commands](https://gitlab.ics.muni.cz/muni-kypo-trainings/datasets/commands). It enables easily adding the commands into a local instance of [ELK](https://www.elastic.co/what-is/elk-stack) (Elasticsearch, Logstash, Kibana) for further analysis.

**Table of contents**
* [Installation](#installation)
* [Usage](#usage)
* [How to cite](#cite)

## Installation

### Requirements

Tested with (should also work with higher versions):

| Technology | Version |
| :---: | :---: |
| [Docker](https://www.docker.com/) | 2.2.0.5+ |
| Docker Engine | 19.03.8+ |
| Docker Compose | 1.25.4+ |
| curl | any |

Tested on the following PC environments:
* Windows 10 Pro, 16 GB RAM, Intel Core i7-8565 CPU
* macOS Big Sur 11.5.2, 16 GB RAM, CPU, Intel Core i5 CPU, 4 GB RAM set for Resources in Docker Desktop

To install and configure Docker on Windows, please follow the documentation of [Docker Desktop](https://docs.docker.com/docker-for-windows/install/).

### Run and Configure ELK

This script will run and configure ELK:
```
$ docker-compose up
```

Or, if you do not want to see detailed output (more difficult for debugging purposes since you won't see errors easily):
```
$ docker-compose up -d
```

The startup takes approximately 30 seconds, provided that you have already downloaded the Elasticsearch, Logstash, and Kibana images. Then, you can access the application in the browser at `http://localhost:9200/` and `http://localhost:5601/`.

## Usage

First, locate the shell command data from the selected event, e.g.: `~/commands/Kobylka 3302` (direct link [here](https://gitlab.ics.muni.cz/muni-kypo-trainings/datasets/commands/-/tree/master/Kobylka%203302)). The data directory must include JSON command history files ending with `-useractions.json`. Other files are ignored.

Then, go to the folder with `insert-events.sh` file and run the `insert-events.sh` script as follows:
```
$ ./insert-events.sh "PATH-TO-YOUR-SHELL-COMMANDS-FOLDER"
```

e.g.,

```
$ ./insert-events.sh "~/commands/Kobylka 3302"
```

This script processes all the files in the given directory and searches for files matching the pattern `*-useractions.json`. Then, it inserts the content of those files under the correct index into local ELK instance running on port 9200. The script will print `ok` for each line that was successfully inserted into ELK.

### Checking the Data in ELK

Visit Kibana user interface at `http://localhost:5601/`. In the menu, move to `Dev Tools` (third button from the bottom with the wrench symbol).

Now, you can execute commands in the `Console`. As an example, use the command below to find the timestamps of the first and the last command in the file.

```
GET logs.console*/_search
{
  "size":0,
  "query": {
    "match": {
      "sandbox_id": 117
    }
  },
  "aggs": {
    "first_event": { "min": {"field":"timestamp_str"}},
    "last_event": { "max": {"field":"timestamp_str"}}
  }
}
```

You should see the result on the right side, like this:

![Screenshot of the ELK interface](elk-example.png)

## License and How to Cite

The software was created by Pavel Seda (seda@fi.muni.cz) and released under the [MIT license](https://gitlab.ics.muni.cz/muni-kypo/tools/commands-elk/-/blob/master/LICENSE). If you use the software, please cite the corresponding data article:

Valdemar Švábenský, Jan Vykopal, Pavel Seda, and Pavel Čeleda.\
*Dataset of Shell Commands Used by Participants of Hands-on Cybersecurity Training.*\
In Elsevier Data in Brief. 2021.
[The article is currently under peer review. DOI/URL links will be added after acceptance.]

```
@article{Svabensky2021dataset,
    author    = {\v{S}v\'{a}bensk\'{y}, Valdemar and Vykopal, Jan and Seda, Pavel and \v{C}eleda, Pavel},
    title     = {{Dataset of Shell Commands Used by Participants of Hands-on Cybersecurity Training}},
    journal   = {{Data in Brief}},
    publisher = {Elsevier},
    year      = {2021},
}
```
